import static org.junit.Assert.*;

import org.junit.Test;

public class AppTest 
{
	@Test
	public void testApp()
    {
        assertEquals(11,new App().add(3,8));
    }
	@Test
	public void testSub()
    {
        assertEquals(4,new App().sub(6,2));
    }
}
